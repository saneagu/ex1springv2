package service;

import bean.CustomerPackage;

public class CustomerService {
	CustomerPackage customerPackage;
	
	public void setCustomerPackage(CustomerPackage customerPackage) {
		this.customerPackage = customerPackage;
	}
	
	public String CustomerServiceType() {
		return customerPackage.serviceType();
	}
	
}
