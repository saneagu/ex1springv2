package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service.CustomerService;

@SpringBootApplication
public class Ex1bisApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ex1bisApplication.class, args);
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		CustomerService service = context.getBean(CustomerService.class);
		
        System.out.println(service.CustomerServiceType());

        context.close();
	}
}



